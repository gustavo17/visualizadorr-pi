#pragma  once

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
namespace gats {
    class VideoConverter {
        public:
            VideoConverter(){}
            int process(const std::string & inputVideoPath, const std::string & outputFilePath,int anchoDeseado){
                cv::VideoCapture inputVideo {inputVideoPath};
                if (!inputVideo.isOpened()) {
                    std::cerr << "No se pudo abrir el video de entrada." << std::endl;
                    return -1;
                }
                double fps = inputVideo.get(cv::CAP_PROP_FPS);
                cv::Size frameSize(static_cast<int>(inputVideo.get(cv::CAP_PROP_FRAME_WIDTH)), 
                       static_cast<int>(inputVideo.get(cv::CAP_PROP_FRAME_HEIGHT))); // Tamaño de los frames originales

                int newHeight=static_cast<double>(anchoDeseado)/frameSize.width*frameSize.height;
                // Crear el video de salida
                cv::VideoWriter outputVideo(outputFilePath, cv::VideoWriter::fourcc('m','p','4','v'), fps, cv::Size(anchoDeseado,newHeight));
                if (!outputVideo.isOpened()) {
                    std::cerr << "No se pudo crear el video de salida." << std::endl;
                    return -1;
                }

                std::cout<<anchoDeseado<<", "<<newHeight<< ", fps:"<<fps<<std::endl;
                cv::Mat frame;
                while (inputVideo.read(frame)) {
                    // Cambiar la resolución del frame (por ejemplo, reducir a la mitad)

                    cv::resize(frame, frame, cv::Size(anchoDeseado,newHeight),0.0,0.0,cv::INTER_CUBIC);

                    // Escribir el frame modificado en el video de salida
                    outputVideo.write(frame);

                }
                // Liberar los recursos
                inputVideo.release();
                outputVideo.release();
                return 0;
            }
    };
}
