#pragma once
#include <opencv2/videoio.hpp>
#include <string>
#include <vector>
#include <fstream>

enum class TIPO_MULT {
    Imagen,
    Video
};

struct MultiMedia {
    std::string file;
    int x;
    int y;
    int width; // el height se calcula en la misma proporcion
    TIPO_MULT tipo;
    cv::VideoCapture *ptr=nullptr; //solo para videos, permite obtener el flujo de frames

    static std::vector<MultiMedia> getVectorMultiMedia(std::string file){
        std::vector<MultiMedia> contenido;
        std::ifstream inputFile {file};
        std::string tipo,x,y,width;
        MultiMedia data;
        while (inputFile >> data.file >> tipo >> x >> y >> width){    
            data.tipo=(tipo=="IMAGEN")?TIPO_MULT::Imagen:TIPO_MULT::Video;
            data.x=std::atoi(x.data());
            data.y=std::atoi(y.data());
            data.width=std::atoi(width.data());
            contenido.push_back(data);
        }
        inputFile.close();
        return contenido;

    }
};