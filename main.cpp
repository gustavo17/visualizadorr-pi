
#include <cstdlib>
#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <string>
#include "multimedia.hpp"
#include "videoConverter.hpp"


//se usa OPENCV en RASPBERRY
//cmake -DCMAKE_BUILD_TYPE=RELEASE -DCMAKE_INSTALL_PREFIX=/usr/local -DWITH_TBB=ON -DWITH_LAPACK=ON -DBUILD_TESTS=OFF -DINSTALL_C_EXAMPLES=OFF -DINSTALL_PYTHON_EXAMPLES=OFF -DBUILD_EXAMPLES=OFF -DOPENCV_EXTRA_MODULES_PATH=/home/gustavotoroschroder/proyectos/opencv_contrib/modules ..

int main() {

    std::vector<MultiMedia> contenido=MultiMedia::getVectorMultiMedia("../contenido.dat");

    //separa vector en imagenes y videos
    std::vector<MultiMedia> imagenes,videos;
    for (auto it=contenido.begin();it!=contenido.end();it++){
        if (it->tipo==TIPO_MULT::Imagen){
            imagenes.push_back(*it);
        } else {
            videos.push_back(*it);
        }
    }    

    //cambiar resolucion de videos
    std::cout<<"preprocesando videos ..."<<std::endl;
    int cont=0;
    for (auto &video:videos){
        std::string nombreVideoOut="video_"+std::to_string(cont++)+".mp4";
        gats::VideoConverter videoConverter;
        if (videoConverter.process(video.file, nombreVideoOut , video.width)!=0) {
            std::cout<<"ERROR AL PREPROCESAR VIDEO "<<video.file<<std::endl;
            exit(0);
        }
        video.file=nombreVideoOut;
    }
    std::cout<<"fin preprocesado videos ..."<<std::endl;
    

    //crea pantalla
    cv::Mat screen=cv::Mat::zeros(cv::Size(3142,1964),CV_8UC3);
    cv::namedWindow("screen",cv::WINDOW_NORMAL);

    //cv::setWindowProperty("screen",cv::WND_PROP_FULLSCREEN, cv::WINDOW_FULLSCREEN);

    //muestra imagenes en screen
    for (auto &img:imagenes) {
        cv::Mat imagen=cv::imread(img.file,cv::IMREAD_COLOR);
        int newHeight=static_cast<double>(img.width)/imagen.cols*imagen.rows;
        cv::resize(imagen, imagen, cv::Size(img.width,newHeight));
        cv::Rect area=cv::Rect(img.x,img.y,imagen.cols,imagen.rows);
        imagen.copyTo(screen(area));
    }

    //init videos
    for (auto &v:videos){
        v.ptr=new cv::VideoCapture(v.file);
    }

    //muestra videos
    int FPS_VISUALIZACION=60;
    for (;;){
        double start = static_cast<double>(cv::getTickCount());
        for (auto &v:videos){
            cv::Mat frame;
            if (!v.ptr->read(frame)){
                v.ptr->release();
                delete v.ptr;
                v.ptr=new cv::VideoCapture(v.file);
                if (!v.ptr->read(frame)){
                    std::cout<<"error al abrir video "<< v.file << std::endl;
                    exit(0);
                }
            }
            cv::Rect area=cv::Rect(v.x,v.y,frame.cols,frame.rows);
            frame.copyTo(screen(area));
        }
        cv::imshow("screen",screen);
        int key=cv::waitKey(1);
        if (key!=-1) break;
        double end = static_cast<double>(cv::getTickCount());
        double duration = (end - start) / cv::getTickFrequency();
        int delay = static_cast<int>((1.0 / FPS_VISUALIZACION - duration) * 1000);
        if (delay>0){
            int key=cv::waitKey(delay);
            if (key!=-1) break;
        }
    }

    //release videos
    for (auto &v:videos){
        if (v.ptr!=nullptr){
            v.ptr->release();
            delete v.ptr;
        }
    }


    cv::destroyAllWindows();
    return 0;
}


